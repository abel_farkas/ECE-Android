package com.example.farkasabel.game;

import android.graphics.Bitmap;

/**
 * Created by Farkas Abel on 1/13/2017.
 */

public class Animation {
    private Bitmap []frames;
    private int currentFrame;
    private long startTime;
    private long delay;
    private boolean palyedOnce;

    public void setFrames(Bitmap[] frames) {
        this.frames = frames;
        currentFrame = 0;
        startTime = System.nanoTime();
    }

    public void setDelay(long delat) {
        this.delay = delat;
    }
    public void setFrame(int i)
    {
        currentFrame = i;
    }
    public void update()
    {
        long elapsed = (System.nanoTime()-startTime)/1000000;
        if(elapsed > delay)
        {
            currentFrame++;
            startTime = System.nanoTime();

        }
        if(currentFrame == frames.length)
        {
            currentFrame =0;
            palyedOnce = true;
        }
    }
    public Bitmap getImage()
    {
        return frames[currentFrame];
    }

    public int getFrame() {
        return currentFrame;
    }

    public boolean palyedOnce() {
        return palyedOnce;
    }
}
