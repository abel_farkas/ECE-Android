package com.example.farkasabel.game;

import android.graphics.Bitmap;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Canvas;
import android.graphics.Matrix;

/**
 * Created by Farkas Abel on 1/13/2017.
 */

public class Background {
    private Bitmap image;
    private int x,y,dx;
    public Background(Bitmap img)
    {
        x=y=0;
        image = img;
    }
    public void update()
    {
        x+=dx;
        if(x<-GamePanel.WIDTH)
        {
            x=0;
        }
    }
    public void draw(Canvas canvas)
    {
        canvas.drawBitmap(image,x,y,null);
        if(x<0)
        {

            canvas.drawBitmap(getResizedBitmap(image,GamePanel.WIDTH,GamePanel.HEIGHT),x+GamePanel.WIDTH,y,null);
        }
    }public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth) {

        int width = bm.getWidth();

        int height = bm.getHeight();

        float scaleWidth = ((float) newWidth) / width;

        float scaleHeight = ((float) newHeight) / height;

// create a matrix for the manipulation

        Matrix matrix = new Matrix();

// resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
// recreate the new Bitmap

        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;

    }
}
