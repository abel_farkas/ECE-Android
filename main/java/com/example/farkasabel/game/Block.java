package com.example.farkasabel.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;

/**
 * Created by Farkas Abel on 1/14/2017.
 */

public class Block {
    private Bitmap block[];

    public Block(Bitmap[] block) {
        this.block = block;
    }
    public Bitmap getBlock(int t,int c)
    {
        Bitmap seged = Bitmap.createBitmap(60,60,Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(seged);
        canvas.drawBitmap(block[t],-c*60,0,null);
        return seged;
    }

}
