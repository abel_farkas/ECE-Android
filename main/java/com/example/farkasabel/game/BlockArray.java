package com.example.farkasabel.game;

/**
 * Created by Farkas Abel on 1/14/2017.
 */

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Rect;

import java.util.ArrayList;

/**
 * Created by Farkas Abel on 12/12/2016.
 */
public class BlockArray implements Drawable {
    ArrayList<BlockPlacer> map;
    ArrayList<BlockPlacer> coin;
    private BlockPlacer fs;
    private BlockPlacer st;
    private Block rajz;
    private Bitmap m;
    private Bitmap c;
    private int tX;
    private int tY;
    private int zoom;
    private int width, height;
    private int maxX, maxY, minX, minY;
    private int gr;
    private int ugr;
    private boolean right;
    private boolean left;
    private boolean u;
    private boolean onTheGround;

    public BlockArray() {
    }


    public BlockArray(Bitmap[] btm, int width, int height) {
        map = new ArrayList<BlockPlacer>();
        coin = new ArrayList<BlockPlacer>();
        rajz = new Block(btm);
        this.width = width;
        this.height = height;
        tX = tY = 0;
        ugr = 0;
        u = false;
    }

    public void moveRigth(boolean m) {//a palya mozgasat kezelo logikai ertekek valtoztatasa
        right = m;
        left = !m;
    }

    public void moveLeft(boolean m) {
        left = m;
        right = !m;
    }

    public void stop() {
        left = right = false;
    }

    public boolean isCollR(int tX, int tY) {//az utkozeseket eszlelo logikai fuggvenyek
        for (BlockPlacer i : map) {
            Rect r = new Rect(i.getX() * 60 + tX, i.getY() * 60 + tY, i.getX() * 60 + 60 + tX, i.getY() * 60 + 60 + tY);
            if (Dude.isLeft(r))
                return true;
        }
        return false;
    }

    public boolean isCollL(int tX, int tY) {
        for (BlockPlacer i : map) {
            Rect r = new Rect(i.getX() * 60 + tX, i.getY() * 60 + tY, i.getX() * 60 + 60 + tX, i.getY() * 60 + 60 + tY);
            if (Dude.isRigth(r))
                return true;
        }
        return false;
    }

    public boolean isCollU(int tX, int tY) {
        for (BlockPlacer i : map) {
            Rect r = new Rect(i.getX() * 60 + tX, i.getY() * 60 + tY, i.getX() * 60 + 60 + tX, i.getY() * 60 + 60 + tY);
            if (Dude.isUp(r))
                return true;
        }
        return false;
    }

    public boolean isCollD(int tX, int tY) {
        for (BlockPlacer i : map) {
            Rect r = new Rect(i.getX() * 60 + tX, i.getY() * 60 + tY, i.getX() * 60 + 60 + tX, i.getY() * 60 + 60 + tY);
            if (Dude.isDown(r))
                return true;
        }
        return false;
    }

    public boolean isCollC(int tX, int tY) {
        for (BlockPlacer i : coin) {
            Rect r = new Rect(i.getX() * 60 + tX, i.getY() * 60 + tY, i.getX() * 60 + 60 + tX, i.getY() * 60 + 60 + tY);
            if (Dude.contains(r)) {
                coin.remove(i);
                return true;
            }
        }
        return false;
    }

    public void init() {//masodlagos inicializacio a beolvasot plyat egy bitmapre rajzolja
        //igy nem kell minden kirajzolasnal bejarni a tombbot
        m = Bitmap.createBitmap((maxX - minX + 1) * 60, (maxY - minY + 1) * 60, Bitmap.Config.ARGB_8888);
        Canvas g = new Canvas(m);
        c = Bitmap.createBitmap((maxX - minX + 1) * 60, (maxY - minY + 1) * 60, Bitmap.Config.ARGB_8888);
        Canvas gc = new Canvas(c);
        for (BlockPlacer i : map) {
            g.drawBitmap(rajz.getBlock(i.c, i.t), i.getX() * 60 - minX * 60, i.getY() * 60 - minY * 60, null);
        }
        for (BlockPlacer i : coin) {
            gc.drawBitmap(rajz.getBlock(i.c, i.t), i.getX() * 60 - minX * 60, i.getY() * 60 - minY * 60, null);
        }
        g.drawBitmap(rajz.getBlock(st.c, st.t), st.getX() * 60 - minX * 60, st.getY() * 60 - minY * 60, null);
        g.drawBitmap(rajz.getBlock(fs.c, fs.t), fs.getX() * 60 - minX * 60, fs.getY() * 60 - minY * 60, null);
    }

    public void initC() {//a felveheto targyak rajzolasat vegzi az init()-hez hasonloan
        Canvas gc = new Canvas(c);
        gc.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        for (BlockPlacer i : coin) {
            gc.drawBitmap(rajz.getBlock(i.c, i.t), i.getX() * 60 - minX * 60, i.getY() * 60 - minY * 60, null);
        }
    }


    public void add(BlockPlacer bl) {//bl-t hozzaadja az arraylisthez
        map.add(bl);
        if (bl.getX() > maxX)
            maxX = bl.getX();
        if (bl.getY() > maxY)
            maxY = bl.getY();
        if (bl.getX() < minX)
            minX = bl.getX();
        if (bl.getY() < minY)
            minY = bl.getY();
    }

    public void addCoin(BlockPlacer bl) {//bl-t felveheto targykent adja hozza az arraylisthez
        coin.add(bl);
        if (bl.getX() > maxX)
            maxX = bl.getX();
        if (bl.getY() > maxY)
            maxY = bl.getY();
        if (bl.getX() < minX)
            minX = bl.getX();
        if (bl.getY() < minY)
            minY = bl.getY();
    }


    public int gravity() {//a gravitaciot kezelo fuggveny
        if (ugr == 0) {
            if (isCollD(tX, tY - gr)) {
                gr = 1;
                ugr = 0;
                return tY + getCloseTy(true);
            } else {
                gr += 1;
                if (gr > 20)
                    gr = 20;
                return tY - gr;
            }
        } else return tY;
    }

    int getCloseTy(boolean p) {//ha utkozest eszlel a program kiszamolja a legkozelebbi y erteket melyre meg nincs utkozes
        if (p) {
            int s = -gr;
            while (isCollD(tX, tY + s))
                s++;
            if (!onTheGround) {
                right = left = false;
                onTheGround = true;
            }
            return s;
        } else {
            int s = ugr;
            while (isCollU(tX, tY + s))
                s--;
            return s;

        }
    }

    public void ugras() {//az ugras gomb ezt a fuggvenyt hivja meg
        if (ugr == 0 && gr == 1) {
            ugr = 20;
        }
    }

    int jump() {//az ugrast vegzo fuggveny
        if (isCollU(tX, tY + ugr) && ugr != 0) {
            tY += getCloseTy(false);
            ugr = 0;
            return tY;
        } else {
            ugr--;
            if (ugr < 0)
                ugr = 0;
            return tY + ugr;
        }
    }

    int getCloseTx(boolean p) {//a getCloseTy()hoz hasonloan mukodik az x ertekre
        if (p) {
            int s = 9;
            while (isCollR(tX + s, tY))
                s--;
            if (s < 0)
                s = 1;
            return s - 1;
        } else {
            int s = -9;
            while (isCollL(tX + s, tY))
                s++;
            if (s > 0)
                s = -1;
            return s + 1;

        }
    }


    public void addStart(BlockPlacer bl) {// a start pozicio hozzaadasahoz szukseges
        tX = -(bl.getX() + 3) * 60 + 1280 / 2;
        tY = -bl.getY() * 60;
        st = bl;
    }

    private void setToStart() {//ha elertunk a plya vegehez a start poziciora allitja a karaktert

        tX = -st.getX() * 60 + 1280 / 2;
        tY = -st.getY() * 60;
    }

    public void addFinish(BlockPlacer bl) {
        fs = bl;
    }//finish hozzadasa

    @Override
    public void draw(Canvas g) {//a kirajzolast vegzo fuggveny
        tY = jump();
        tY = gravity();
        if (right)
            if (!isCollR(tX + 10, tY))
                tX += 10;
            else {
                tX += getCloseTx(true);
            }
        if (left)
            if (!isCollL(tX - 10, tY))
                tX -= 10;
            else {
                tX += getCloseTx(false);
            }
        if (isCollC(tX, tY)) {
            initC();
        }
        g.drawBitmap(m, minX * 60 + tX, minY * 60 + tY, null);
        g.drawBitmap(c, minX * 60 + tX, minY * 60 + tY, null);
        if (Dude.contains(new Rect(fs.getX() * 60 + tX, fs.getY() * 60 + tY, fs.getX() * 60 + 60 + tX, fs.getY() * 60 + 60 + tY))) {
            setToStart();
        }

    }

    public void clear() {
        map.removeAll(map);
    }


    @Override
    public void update() {

    }
}
