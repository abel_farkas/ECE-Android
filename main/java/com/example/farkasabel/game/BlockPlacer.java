package com.example.farkasabel.game;

/**
 * Created by Farkas Abel on 1/14/2017.
 */
public class BlockPlacer {
    int x;
    int y;
    int t;
    int c;
    public BlockPlacer(int x, int y, int t,int c) {
        this.x = x;
        this.y = y;
        this.t = t;
        this.c = c;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getT() {
        return t;
    }
    public int getC() {
        return c;
    }

    @Override
    public String toString() {
        return x+" "+y+" "+t+" "+c;
    }
}
