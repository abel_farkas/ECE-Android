package com.example.farkasabel.game;

import android.app.usage.UsageEvents;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.view.MotionEvent;

/**
 * Created by Farkas Abel on 1/13/2017.
 */

public class Button implements Drawable {

    Rect rect;
    boolean isPressed;
    int pressedIndex;

    public void setPressedIndex(int pressedIndex) {
        this.pressedIndex = pressedIndex;
    }

    public int getPressedIndex() {

        return pressedIndex;
    }

    public Button(Rect rect)
    {

        this.rect = rect;
    }
    private void drawBackground(Canvas canvas) {
    }
    public void draw(Canvas canvas)
    {
        Paint p = new Paint();
        if(!isPressed)
            p.setColor(Color.rgb(100, 100, 100));
        else
            p.setColor(Color.rgb(255, 255, 255));
        p.setAlpha(125);
        canvas.drawRect(rect,p);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeWidth(4);
        p.setColor(Color.rgb(255, 255, 255));
        canvas.drawRect(rect,p);
        p.setStrokeWidth(2);
        p.setColor(Color.rgb(100, 100, 100));
        canvas.drawRect(rect,p);
    }
    public boolean isContains(int x,int y)
    {
        return rect.contains(x,y);
    }

    @Override
    public void update() {

    }

    public boolean isPressed() {
        return isPressed;
    }

    public void setPressed(boolean pressed) {
        isPressed = pressed;
    }

    public Rect getRect() {
        return rect;
    }
}
