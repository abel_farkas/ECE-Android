package com.example.farkasabel.game;

import android.graphics.Canvas;

/**
 * Created by Farkas Abel on 1/13/2017.
 */

public interface Drawable {
    void draw(Canvas canvas);
    void update();
}
