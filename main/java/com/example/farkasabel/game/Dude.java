package com.example.farkasabel.game;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;

/**
 * Created by Farkas Abel on 1/13/2017.
 */

public class Dude extends GameObject implements Drawable {
    private Bitmap dude;
    private Bitmap duder;
    private static int x;
    private static int y;
    private int dir;
    private boolean moving;
    private static Rect rect;

    public void setMoving(boolean moving) {
        this.moving = moving;
    }

    public Dude(int x, int y, Bitmap dude, Bitmap duder) {
        this.dude = dude;
        this.duder = duder;
        this.x = x;
        this.y = y;
        rect = new Rect(x+20,y,x+59,y+117);
    }
    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {

        this.dir = dir;
    }

    @Override
    public void draw(Canvas canvas) {
        if (dir == 1) {
            canvas.drawBitmap(dude, x, y, null);

        } else {
            canvas.drawBitmap(duder, x, y, null);
        }
    }

    @Override
    public void update() {

    }

    public static boolean isUp(Rect r)
    {
        return rect.top<r.bottom&&Rect.intersects(rect,r);
    }

    public static boolean isDown(Rect r)
    {
        return rect.bottom>r.top&&Rect.intersects(rect,r);
    }
    public static boolean isLeft(Rect r)
    {
        return rect.left<r.right&&Rect.intersects(rect,r);
    }

    public static boolean isRigth(Rect r)
    {
        return rect.right>r.left&&Rect.intersects(rect,r);
    }


    public static boolean contains(Rect r) {
        return Rect.intersects(rect,r);
    }
}
