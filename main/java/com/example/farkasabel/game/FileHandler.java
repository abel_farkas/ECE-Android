package com.example.farkasabel.game;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created by Farkas Abel on 1/14/2017.
 */

public class FileHandler {

    public static void beMap(BlockArray a,InputStream is)
    {
        //beolvasas
        BufferedReader in = new BufferedReader(new InputStreamReader(is));

        int [] tall = new int [100];

        String s = null;
        try {
            s = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        int i = 0;
        while(s!=null)//vegigmegy a beolvasott fileon
        {
            String[] items = s.split(" ");//szetosztja a beolvasott sort negy stringre
            int tulaj[] = new int[4];
            try {
                for(int j=0;j<4;j++)
                    tulaj[j] = Integer.parseInt(items[j]);
                if(tulaj[3]<2)//ha a beolvasott ertekek nem kulonleges blockot adnak hozzaadja a plyahoz
                    a.add(new BlockPlacer(tulaj[0],tulaj[1],tulaj[2],tulaj[3]));
                else{
                    if(tulaj[2]==1)//ha a kulonleges block a start pozicio
                        a.addStart(new BlockPlacer(tulaj[0],tulaj[1],tulaj[2],tulaj[3]));
                    else if(tulaj[2]==2)//ha a kulonleges block a finiish pozicio
                        a.addFinish(new BlockPlacer(tulaj[0],tulaj[1],tulaj[2],tulaj[3]));
                    else a.addCoin(new BlockPlacer(tulaj[0],tulaj[1],tulaj[2],tulaj[3]));
                    //hozaadja a gyujtheto elemeket is
                }
                i++;
            }catch (NumberFormatException|ArrayIndexOutOfBoundsException e)
            {
            }

            try {
                s = in.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
