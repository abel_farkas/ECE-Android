package com.example.farkasabel.game;

import android.app.Activity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import static android.view.WindowManager.*;

public class Game extends Activity {
    GamePanel gM;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       //title bar kikapcsolása
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        //fullscreen mód
        getWindow().setFlags(LayoutParams.FLAG_FULLSCREEN, LayoutParams.FLAG_FULLSCREEN);
        gM = new GamePanel(this);
        setContentView(gM);
        //a kepernyo elalvasanak megakadalyozasa
        gM.setKeepScreenOn(true);
    }
}
