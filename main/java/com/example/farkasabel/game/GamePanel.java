package com.example.farkasabel.game;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/**
 * Created by Farkas Abel on 1/13/2017.
 */

public class GamePanel extends SurfaceView implements SurfaceHolder.Callback, Drawable {
    public static final int WIDTH = 856;
    public static final int HEIGHT = 480;
    private MainThread thread;
    private Background bg;
    private Button b;
    private JoiStick j;
    private Dude dude;
    private BlockArray bA;
    boolean dudeM;
    boolean dudeD;

    public GamePanel(Context context) {//szukseges inicializalasok
        super(context);
        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);
        setFocusable(true);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        //a surfaceholder.callback interface egy metodusa nem hasznalom a programhoz
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        //a surfaceholder.callback interface egy metodusa a program kilepesekor megszunteti a letrehozott threadet
        // es a reszeit atadja a garbagecollectornak ami uriti a memoriat
        boolean retry = true;
        while (retry)
            try {
                thread.setRunning(false);
                thread.join();
            } catch (InterruptedException r) {
                retry = false;
                r.printStackTrace();
            }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        //a surfaceholder.callback interface egy metodusa a program inditasakor ez fut le eloszor
        //a szukseges inicializalasokat vegzem itt
        b = new Button(new Rect(100, 400, 200, 500));
        //az ugras gomb inicializalasa
        j = new JoiStick(850, 400);
        //a joysick inicializalasa
        dudeM = false;
        dudeD = true;
        //ket valtozo amely megadja a karakter mozgasi iranyat es hogy epp mozog e
        Bitmap[] seged = new Bitmap[3];
        seged[0] = BitmapFactory.decodeResource(getResources(), R.drawable.bricks);
        seged[1] = BitmapFactory.decodeResource(getResources(), R.drawable.dirt);
        seged[2] = BitmapFactory.decodeResource(getResources(), R.drawable.other);
        // kulombozo bitmapek betoltese egy seged tombbe
        bA = new BlockArray(seged, getWidth(), getHeight());
        // a BlockArray a palyat tartalmazza annak az elsodleges inicializalasa

        FileHandler.beMap(bA, getResources().openRawResource(R.raw.map));
        // a BlockArray feltolteset vegzi

        bA.init();
        // a BlockArray masodlagos inicializalasa a program hatekonyabb futasat segiti

        bg = new Background(BitmapFactory.decodeResource(getResources(), R.drawable.bgfar));
        // hatter inicializalasa
        dude = new Dude(getWidth() / 2, getHeight() / 2,
                BitmapFactory.decodeResource(getResources(), R.drawable.dude),
                BitmapFactory.decodeResource(getResources(), R.drawable.duder));
        //a karakter inicializalasa
        if (thread.getState() == Thread.State.TERMINATED) {//a main thread inicializalasa es elinditasa
            thread = new MainThread(getHolder(), this);
            thread.setRunning(true);
            thread.start();
        } else {
            thread.setRunning(true);
            thread.start();

        }
    }


    void setDir(int dir) {//a joysticktol kapott ertekek szerint allitja be a karakter es a palya mozgatasat
        switch (dir) {
            case 2:
                dudeM = false;
                dude.setMoving(false);
                break;
            case 1:
                dude.setDir(1);
                dude.setMoving(true);
                dudeD = false;
                dudeM = true;
                break;
            case 0:
                dude.setDir(1);
                dude.setMoving(true);
                dudeD = false;
                dudeM = true;
                break;
            case 7:
                dude.setDir(1);
                dude.setMoving(true);
                dudeD = false;
                dudeM = true;
                break;
            case 3:
                dude.setDir(-1);
                dude.setMoving(true);
                dudeD = true;
                dudeM = true;
                break;
            case 4:
                dude.setDir(-1);
                dude.setMoving(true);
                dudeD = true;
                dudeM = true;
                break;
            case 5:
                dude.setDir(-1);
                dude.setMoving(true);
                dudeD = true;
                dudeM = true;
                break;
            default:
                dudeM = false;
                dude.setMoving(false);
                break;
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {//a kepernyo erinteset kezelo fuggveny
        int action = event.getActionMasked();
        int index = event.getActionIndex();
        switch (action) {

            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_POINTER_DOWN:
                //akkor lep be ebbe az agba ha egy ujj hozzaer a kepernyohoz
                //ha mar van(nak) ujj(ak) mi(k) erinti(k) a kepernyot akkor is belep
                if (b.isContains((int) event.getX(index), (int) event.getY(index))) {
                    b.setPressed(true);
                    bA.ugras();
                }
                if (j.distancer((int) event.getX(index), (int) event.getY(index)) < 100) {
                    j.setJoi((int) event.getX(index), (int) event.getY(index));
                    setDir(j.getDir());
                }
                break;

            case MotionEvent.ACTION_MOVE:
                //ha egy ujj mozog a kepernyot erintve
                if (event.getPointerCount() > 1) {
                    for (int t = 0; t < event.getPointerCount(); t++) {
                        int id = event.findPointerIndex(t);
                        try {
                            if (id >= 0)
                                if (j.distancer((int) event.getX(id), (int) event.getY(id)) < 200) {
                                    //akkor lep be ha a joystick megadott tavolsagaban van mozgas
                                    j.setJoi((int) event.getX(id), (int) event.getY(id));
                                    setDir(j.getDir());
                                }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }else
                {
                    if (j.distancer((int) event.getX(), (int) event.getY()) < 200) {
                        j.setJoi((int) event.getX(), (int) event.getY());
                        setDir(j.getDir());
                    }
                }
                break;

            case MotionEvent.ACTION_POINTER_UP:
            case MotionEvent.ACTION_UP:
                //ha egy ujj elhagyja a kepernyot
                if (b.isContains((int) event.getX(index), (int) event.getY(index))) {
                    b.setPressed(false);
                } else if (j.distancer((int) event.getX(index), (int) event.getY(index)) < 200) {
                    j.resetJoi();
                    setDir(j.getDir());
                    j.setPointerIndex(-1);
                }
                break;
        }
        return true;
    }

    public void update() {
        dude.update();
    }

    @Override
    public void draw(Canvas canvas) {
        //a kirajzolast vegzo fuggveny
        final float scaleX = getWidth() / (1280 * 1.0f);
        final float scaleY = getHeight() / (720 * 1.0f);
        if (canvas != null) {
            final int savedState = canvas.save();
            canvas.scale(scaleX, scaleY);
            bg.draw(canvas);
            bA.draw(canvas);
            //bA.isColl(dude.getRect());
            if (dudeM) {
                if (dudeD) {
                    bA.moveRigth(true);
                } else {
                    bA.moveLeft(true);
                }
            } else {
                bA.stop();
            }
            dude.draw(canvas);
            canvas.restoreToCount(savedState);
            b.draw(canvas);
            j.draw(canvas);
        }
    }
}
