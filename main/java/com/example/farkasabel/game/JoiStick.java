package com.example.farkasabel.game;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

/**
 * Created by Farkas Abel on 1/13/2017.
 */

public class JoiStick implements Drawable {
    private int x;
    private int y;
    private int px;
    private int py;
    private int pointerIndex;


    public int getPointerIndex() {
        return pointerIndex;
    }

    public void setPointerIndex(int pointerIndex) {
        this.pointerIndex = pointerIndex;
    }

    public JoiStick(int x, int y) {
        this.x = x;
        this.y = y;
        px = 0;
        py = 0;
    }

    public void setJoi(int px, int py) {
        this.px = px;
        this.py = py;
        getRealJoi();
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }


    public int getDir() {
        double szog = 0;
        /**
         * 0 jobb
         * 1 jobb fel
         * 2 fel
         * 3 bal fel
         * 4 bal
         * 5 bal le
         * 6 le
         * 7 jobb le
         * -1 semmi
         */

        if (Math.toDegrees(Math.asin((float) -py / 100.0f)) > 0) {
            szog = Math.toDegrees(Math.acos((float) px / 100.0f));
        } else
            szog = 180 - Math.toDegrees(Math.acos((float) px / 100.0f)) + 180;
        //System.out.println(Math.toDegrees(Math.acos((float)px/100.0f))+" "+Math.toDegrees(Math.asin((float)-py/100.0f)));
        //System.out.println(szog);
        if (distance(px, py) > 50) {
            int irany = (int) ((szog) / 22.5f);
            if ((irany + 1) / 2 < 8)
                return (irany + 1) / 2;
            else return 0;
        } else
            return -1;
    }

    private void getRealJoi() {
        px = -(x - px);
        py = -(y - py);
        if (distance(px, py) > 100) {
            float rat = 1;
            rat = 100 / distance(px, py);
            px *= rat;
            py *= rat;
        }
    }

    public void resetJoi() {
        px = py = 0;
    }

    public float distance(int x1, int y1) {
        float s = ((x1) * (x1)) + ((y1) * (y1));
        return (float) Math.sqrt(s);
    }

    public float distancer(int x1, int y1) {
        float s = ((x1 - x) * (x1 - x)) + ((y1 - y) * (y1 - y));
        return (float) Math.sqrt(s);
    }

    private void drawBackground(Canvas canvas) {
        Paint p = new Paint();
        p.setColor(Color.rgb(255, 255, 255));
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeWidth(3.0f);
        canvas.drawCircle(x, y, 100, p);
        p.setColor(Color.rgb(100, 100, 100));
        canvas.drawCircle(x, y, 97, p);
        p.setColor(Color.rgb(100, 100, 100));
        p.setStyle(Paint.Style.FILL);
        p.setAlpha(125);
        canvas.drawCircle(x, y, 97, p);
    }

    private void drawJoi(Canvas canvas) {
        Paint p = new Paint();
        p.setColor(Color.rgb(255, 255, 255));
        p.setStyle(Paint.Style.FILL);
        canvas.drawCircle(x + px, y + py, 50, p);
        p.setStyle(Paint.Style.STROKE);
        p.setStrokeWidth(3.0f);
        canvas.drawCircle(x + px, y + py, 47, p);
        p.setColor(Color.rgb(100, 100, 100));
        canvas.drawCircle(x + px, y + py, 50, p);

    }

    public void draw(Canvas canvas) {
        drawBackground(canvas);
        drawJoi(canvas);
    }

    @Override
    public void update() {
    }

}
